package com.eshop.fulfill.domain.event;

import lombok.Data;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单已配送物流结果消息
 * @date 2022-04-06
 */
@Data
public class OrderDeliveredWmsEvent extends BaseWmsShipEvent{
    /**
     * 配送员code
     */
    private String delivererNo;
    /**
     * 配送员姓名
     */
    private String delivererName;
    /**
     * 配送员手机号
     */
    private String delivererPhone;
}
