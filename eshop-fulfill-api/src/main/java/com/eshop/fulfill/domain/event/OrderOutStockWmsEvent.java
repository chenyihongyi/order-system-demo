package com.eshop.fulfill.domain.event;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单已出库物流结果消息
 * @date 2022-04-06
 */
@Data
public class OrderOutStockWmsEvent extends BaseWmsShipEvent {
    /**
     * 出库时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date outStockTime;
}
