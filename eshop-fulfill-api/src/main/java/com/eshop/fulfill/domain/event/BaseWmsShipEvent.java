package com.eshop.fulfill.domain.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流配送结果事件基类
 * @date 2022-04-06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BaseWmsShipEvent implements Serializable {

    private static final long serialVersionUID = 3310845067561671265L;

    /**
     * 订单编号
     */
    private String orderId;
}
