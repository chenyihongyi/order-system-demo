package com.eshop.fulfill.domain.event;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单已签收物流结果消息
 * @date 2022-04-06
 */
@Data
public class OrderSignedWmsEvent extends BaseWmsShipEvent{
    /**
     * 签收事件
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date signedTime;
}
