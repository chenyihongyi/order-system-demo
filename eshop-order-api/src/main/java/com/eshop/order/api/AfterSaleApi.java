package com.eshop.order.api;

import com.eshop.common.core.JsonResult;
import com.eshop.customer.domain.request.CustomerReviewReturnGoodsRequest;
import com.eshop.order.domain.dto.LackDTO;
import com.eshop.order.domain.request.CancelOrderRequest;
import com.eshop.order.domain.request.LackRequest;
import com.eshop.order.domain.request.RefundCallbackRequest;
import com.eshop.order.domain.request.RevokeAfterSaleRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单中心-逆向售后业务接口
 * @date 2022-04-08
 */
public interface AfterSaleApi {

    /**
     * 取消订单/超时未支付取消
     */
    JsonResult<Boolean> cancelOrder(CancelOrderRequest cancelOrderRequest);

    /**
     * 缺品
     */
    JsonResult<LackDTO> lockItem(LackRequest request);

    /**
     * 取消订单支付退款回调
     */
    JsonResult<Boolean> refundCallback(RefundCallbackRequest payRefundCallbackRequest);

    /**
     * 接收客服的审核结果
     */
    JsonResult<Boolean> receiveCustomerAuditResult(CustomerReviewReturnGoodsRequest customerReviewReturnGoodsRequest);

    /**
     * 用户撤销售后申请
     */
    JsonResult<Boolean> revokeAfterSale(RevokeAfterSaleRequest request);

}
