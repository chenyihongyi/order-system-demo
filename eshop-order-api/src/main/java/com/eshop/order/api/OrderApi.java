package com.eshop.order.api;


import com.eshop.common.core.JsonResult;
import com.eshop.order.domain.dto.CreateOrderDTO;
import com.eshop.order.domain.dto.GenOrderIdDTO;
import com.eshop.order.domain.dto.PrePayOrderDTO;
import com.eshop.order.domain.request.CreateOrderRequest;
import com.eshop.order.domain.request.GenOrderIdRequest;
import com.eshop.order.domain.request.PayCallbackRequest;
import com.eshop.order.domain.request.PrePayOrderRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单中心-正向下单业务接口
 * @date 2022-04-05
 */
public interface OrderApi {

    /**
     * 生成订单号接口
     *
     * @param genOrderIdRequest 生成订单号入参
     * @return 订单号
     */
    JsonResult<GenOrderIdDTO> genOrderId(GenOrderIdRequest genOrderIdRequest);

    /**
     * 提交订单接口
     * @param createOrderRequest 提交订单请求入参
     * @return 订单号
     */
    JsonResult<CreateOrderDTO> createOrder(CreateOrderRequest createOrderRequest);

    /**
     * 预支付订单接口
     * @param prePayOrderRequest 预支付订单请求入参
     * @return
     */
    JsonResult<PrePayOrderDTO> prePayOrder(PrePayOrderRequest prePayOrderRequest);

    /**
     * 支付回调接口
     * @param payCallbackRequest 支付系统回调入参
     * @return
     */
    JsonResult<Boolean> payCallback(PayCallbackRequest payCallbackRequest);
}
