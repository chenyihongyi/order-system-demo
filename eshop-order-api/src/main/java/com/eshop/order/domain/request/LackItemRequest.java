package com.eshop.order.domain.request;

import com.eshop.common.core.AbstractObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Elvis
 * @version 1.0
 * @description:具体的缺品项
 * @date 2022-04-06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LackItemRequest extends AbstractObject implements Serializable {

    /**
     * sku编码
     */
    private String skuCode;

    /**
     * 缺品数量
     */
    private Integer lackNum;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LackItemRequest that = (LackItemRequest) o;
        return Objects.equals(skuCode, that.skuCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(skuCode);
    }
}

