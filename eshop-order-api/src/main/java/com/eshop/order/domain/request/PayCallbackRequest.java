package com.eshop.order.domain.request;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:支付系统回调请求对象
 * @date 2022-04-06
 */
@Data
public class PayCallbackRequest extends AbstractObject implements Serializable {

    private static final long serialVersionUID = 3685085492927992753L;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 支付账户
     */
    private String payAccount;

    /**
     * 支付金额
     */
    private Integer payAmount;

    /**
     * 支付系统交易单号
     */
    private String outTradeNo;

    /**
     * 支付方式
     */
    private Integer payType;

    /**
     * 商户号
     */
    private String merchantId;

    /**
     * 支付渠道
     */
    private String payChannel;

    /**
     * 微信平台 appid
     */
    private String appid;

}