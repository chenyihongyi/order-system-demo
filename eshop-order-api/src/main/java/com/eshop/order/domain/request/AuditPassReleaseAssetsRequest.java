package com.eshop.order.domain.request;

import com.eshop.common.core.AbstractObject;
import com.eshop.common.message.ActualRefundMessage;
import com.eshop.order.domain.dto.ReleaseProductStockDTO;
import lombok.Data;

import java.io.Serializable;


/**
 * @author Elvis
 * @version 1.0
 * @description: 客服审核通过后发送释放资产入参
 * @date 2022-04-08
 */
@Data
public class AuditPassReleaseAssetsRequest extends AbstractObject implements Serializable {
    /**
     * 释放库存DTO
     */
    private ReleaseProductStockDTO releaseProductStockDTO;

    /**
     * 实际退款message数据
     */
    private ActualRefundMessage actualRefundMessage;
}
