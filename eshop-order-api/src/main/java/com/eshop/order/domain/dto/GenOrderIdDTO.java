package com.eshop.order.domain.dto;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@Data
public class GenOrderIdDTO extends AbstractObject implements Serializable {

    /**
     * 订单号
     */
    private String orderId;
}
