package com.eshop.order.domain.request;

import lombok.Data;

/**
 * @author Elvis
 * @version 1.0
 * @description: 用户撤销售后申请
 * @date 2022-04-06
 */
@Data
public class RevokeAfterSaleRequest {
    /**
     * 售后单
     */
    private Long afterSaleId;
}
