package com.eshop.order.domain.dto;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单费用明细表
 * @date 2022-04-05
 */
@Data
public class OrderAmountDetailDTO extends AbstractObject implements Serializable {

    private static final long serialVersionUID = 5812183258654799741L;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 产品类型
     */
    private Integer productType;

    /**
     * sku编码
     */
    private String skuCode;

    /**
     * 销售数量
     */
    private Integer saleQuantity;

    /**
     * 销售单价
     */
    private Integer salePrice;

    /**
     * 收费类型
     */
    private Integer amountType;

    /**
     * 收费金额
     */
    private Integer amount;
}
