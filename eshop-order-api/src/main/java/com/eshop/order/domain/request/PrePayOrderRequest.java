package com.eshop.order.domain.request;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@Data
public class PrePayOrderRequest extends AbstractObject implements Serializable {

    private static final long serialVersionUID = -634137320435888212L;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 业务方标识
     */
    private String businessIdentifier;

    /**
     * 支付类型
     */
    private Integer payType;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 订单支付金额
     */
    private Integer payAmount;

    /**
     * 支付成功后跳转地址
     */
    private String callbackUrl;

    /**
     * 支付失败跳转地址
     */
    private String callbackFailUrl;

    /**
     * 微信openid
     */
    private String openid;

    /**
     * 订单摘要
     */
    private String subject;

    /**
     * 商品明细 json
     */
    private String itemInfo;

}
