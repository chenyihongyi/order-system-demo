package com.eshop.order.domain.dto;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-08
 */
@Data
public class ReleaseProductStockDTO extends AbstractObject implements Serializable {
    private static final long serialVersionUID = 4202385798844939307L;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 订单条目
     */
    private List<OrderItemRequest> orderItemRequestList;

    @Data
    public static class OrderItemRequest extends AbstractObject implements Serializable {

        private static final long serialVersionUID = 6870559288334853954L;

        /**
         * 商品sku编号
         */
        private String skuCode;

        /**
         * 销售数量
         */
        private Integer saleQuantity;

    }

}