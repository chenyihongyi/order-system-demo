package com.eshop.order.domain.request;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单缺品请求
 * @date 2022-04-08
 */
@Data
public class LackRequest extends AbstractObject implements Serializable {

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 具体的缺品项
     */
    private Set<LackItemRequest> lackItems;

}
