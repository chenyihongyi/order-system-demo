package com.eshop.order.domain.dto;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 取消订单 退款金额 DTO
 * @date 2022-04-06
 */
@Data
public class CancelOrderRefundAmountDTO extends AbstractObject implements Serializable {
    private static final long serialVersionUID = 2078305514048894973L;
    /**
     * 订单号
     */
    private String orderId;
    /**
     * 交易总金额
     */
    private Integer totalAmount;
    /**
     * 实际退款金额
     */
    private Integer returnGoodAmount;


    /**
     * 申请退款金额
     */
    private Integer applyRefundAmount;

    /**
     * sku编号
     */
    private String skuCode;
    /**
     * 退货数量
     */
    private Integer returnNum;


}
