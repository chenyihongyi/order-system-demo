package com.eshop.order.domain.dto;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 创建订单返回结果
 * @date 2022-04-05
 */
@Data
public class CreateOrderDTO extends AbstractObject implements Serializable {

    /**
     * 订单ID
     */
    private String orderId;

    // 库存不足的商品列表
}
