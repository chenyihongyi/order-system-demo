package com.eshop.risk.domain;

import com.eshop.common.core.AbstractObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 发货结果
 * @date 2022-04-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendOutDTO implements Serializable {
    /**
     * 订单ID
     */
    private String orderId;
    /**
     * 物流单号
     */
    private String logisticsCode;
}