package com.eshop.risk.api;

import com.eshop.common.core.JsonResult;
import com.eshop.risk.domain.SendOutDTO;
import com.eshop.risk.domain.SendOutRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-08
 */
public interface TmsApi {

    /**
     * 发货
     */
    JsonResult<SendOutDTO> sendOut(SendOutRequest request);

    /**
     * 取消发货
     */
    JsonResult<Boolean> cancelSendOut(String orderId);
}