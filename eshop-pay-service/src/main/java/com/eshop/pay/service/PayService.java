package com.eshop.pay.service;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface PayService {

    /**
     * 实时查询支付交易流水号
     */
    Boolean getRealTimeTradeNo(String orderId, Integer businessIdentifier);
}
