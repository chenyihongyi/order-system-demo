package com.eshop.customer.api;


import com.eshop.common.core.JsonResult;
import com.eshop.customer.domain.request.CustomerReviewReturnGoodsRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:客服中心业务接口
 * @date 2022-04-06
 */
public interface CustomerApi {

    /**
     * 客服接收审核售后申请
     */
    JsonResult<Boolean> customerAudit(CustomerReviewReturnGoodsRequest customerReviewReturnGoodsRequest);
}