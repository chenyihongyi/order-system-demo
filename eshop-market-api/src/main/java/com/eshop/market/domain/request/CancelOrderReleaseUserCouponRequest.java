package com.eshop.market.domain.request;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:取消订单 释放优惠券入参
 * @date 2022-04-06
 */
@Data
public class CancelOrderReleaseUserCouponRequest extends AbstractObject implements Serializable {

    private static final long serialVersionUID = -1002367548096870904L;
    /**
     * 接入方业务线标识  1, "自营商城"
     */
    private Integer businessIdentifier;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 使用的优惠券编号
     */
    private String couponId;
}
