package com.eshop.market.api;

import com.eshop.common.core.JsonResult;
import com.eshop.market.domain.dto.CalculateOrderAmountDTO;
import com.eshop.market.domain.dto.UserCouponDTO;
import com.eshop.market.domain.query.UserCouponQuery;
import com.eshop.market.domain.request.CalculateOrderAmountRequest;
import com.eshop.market.domain.request.LockUserCouponRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface MarketApi {

    /**
     * 查询用户的优惠券
     * @param userCouponQuery
     * @return
     */
    JsonResult<UserCouponDTO> getUserCoupon(UserCouponQuery userCouponQuery);

    /**
     * 计算订单费用
     *
     * @param calculateOrderAmountRequest
     * @return
     */
    JsonResult<CalculateOrderAmountDTO> calculateOrderAmount(CalculateOrderAmountRequest calculateOrderAmountRequest);

    /**
     * 锁定用户优惠券记录
     *
     * @param lockUserCouponRequest
     * @return
     */
    JsonResult<Boolean> lockUserCoupon(LockUserCouponRequest lockUserCouponRequest);
}
