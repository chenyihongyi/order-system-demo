package com.eshop.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author Elvis
 * @version 1.0
 * @description: 自定义全局过滤器
 * @date 2022-04-08
 */
@Component
public class LoginGlobalFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        // 获取请求令牌
        String token = exchange.getRequest().getHeaders().getFirst("token");

        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
