package com.eshop.inventory.api;

import com.eshop.common.core.JsonResult;
import com.eshop.inventory.domain.request.LockProductStockRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface InventoryApi {

    /**
     * 锁定商品库存
     *
     * @param lockProductStockRequest
     * @return
     */
    JsonResult<Boolean> lockProductStock(LockProductStockRequest lockProductStockRequest);
}
