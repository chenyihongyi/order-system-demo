package com.eshop.inventory.domain.request;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description:取消订单 释放商品库存入参
 * @date 2022-04-06
 */
@Data
public class CancelOrderReleaseProductStockRequest extends AbstractObject implements Serializable {
    private static final long serialVersionUID = 8108365293403422149L;

    /**
     * 订单包含的sku list
     */
    private List<String> skuCodeList;

    /**
     * 订单编号
     */
    private String orderId;

    /**
     * 接入方业务线标识  1, "自营商城"
     */
    private Integer businessIdentifier;

}