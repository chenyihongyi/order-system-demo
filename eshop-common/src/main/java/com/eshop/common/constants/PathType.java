package com.eshop.common.constants;

/**
 * @author Elvis
 * @version 1.0
 * @description: 文件路径类型
 * @date 2022-04-05
 */
public class PathType {

    /**
     * 相对路径
     */
    public static final String RELATIVE = "relative";
    /**
     * 绝对路径
     */
    public static final String ABSOLUTE = "absolute";

    private PathType() {

    }

}
