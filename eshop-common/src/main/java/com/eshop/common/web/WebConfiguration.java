package com.eshop.common.web;

import com.eshop.common.core.DateProvider;
import com.eshop.common.core.DateProviderImpl;
import com.eshop.common.core.ObjectMapperImpl;
import com.eshop.common.exception.GlobalExceptionHandler;
import com.eshop.common.json.JsonExtractor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Elvis
 * @version 1.0
 * @description: web相关bean组件配置
 * @date 2022-04-05
 */
@Configuration
@Import(value = {GlobalExceptionHandler.class, GlobalResponseBodyAdvice.class})
public class WebConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapperImpl();
    }

    @Bean
    public DateProvider dateProvider() {
        return new DateProviderImpl();
    }

    @Bean
    public JsonExtractor jsonExtractor() {
        return new JsonExtractor();
    }
}
