package com.eshop.common.core;

/**
 * @author Elvis
 * @version 1.0
 * @description: 克隆方向枚举
 * @date 2022-04-05
 */
public enum CloneDirection {

    /**
     * 正向克隆：从VO->DTO，DTO->DO
     */
    FORWARD(1),
    /**
     * 反向克隆：从DO->DTO，DTO->VO
     */
    OPPOSITE(2);

    private Integer code;

    CloneDirection(Integer code) {
        this.code = code;
    }

}