package com.eshop.common.message;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:订单完成支付消息
 * @date 2022-04-05
 */
@Data
public class PaidOrderSuccessMessage implements Serializable {

    private static final long serialVersionUID = 2575864833116171389L;

    private String orderId;
}
