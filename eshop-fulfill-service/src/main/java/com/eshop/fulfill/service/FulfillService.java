package com.eshop.fulfill.service;

import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 履约service
 * @date 2022-04-08
 */
public interface FulfillService {

    /**
     * 创建履约单
     * @param request
     */
    void createFulfillOrder(ReceiveFulfillRequest request);

    /**
     * 取消履约单
     * @param orderId
     */
    void cancelFulfillOrder(String orderId);

    /**
     * 触发履约
     */
    Boolean receiveOrderFulFill(ReceiveFulfillRequest request);

}
