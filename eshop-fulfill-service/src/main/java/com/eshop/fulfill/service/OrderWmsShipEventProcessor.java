package com.eshop.fulfill.service;

import com.eshop.fulfill.domain.request.TriggerOrderWmsShipEventRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单物流配送结果处理器
 * @date 2022-04-08
 */
public interface OrderWmsShipEventProcessor {

    /**
     * 执行
     * @param request
     */
    void execute(TriggerOrderWmsShipEventRequest request);

}
