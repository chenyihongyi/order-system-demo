package com.eshop.fulfill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.fulfill.domain.entity.OrderFulfillDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单履约表 Mapper 接口
 * @date 2022-04-08
 */
@Mapper
public interface OrderFulfillMapper extends BaseMapper<OrderFulfillDO> {

}