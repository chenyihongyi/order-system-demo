package com.eshop.fulfill.exception;

import com.eshop.common.exception.BaseBizException;
import com.eshop.common.exception.BaseErrorCodeEnum;

/**
 * @author Elvis
 * @version 1.0
 * @description: 履约中心自定义业务异常类
 * @date 2022-04-08
 */
public class FulfillBizException extends BaseBizException {

    public FulfillBizException(String errorMsg) {
        super(errorMsg);
    }

    public FulfillBizException(String errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public FulfillBizException(BaseErrorCodeEnum baseErrorCodeEnum) {
        super(baseErrorCodeEnum);
    }

    public FulfillBizException(String errorCode, String errorMsg, Object... arguments) {
        super(errorCode, errorMsg, arguments);
    }

    public FulfillBizException(BaseErrorCodeEnum baseErrorCodeEnum, Object... arguments) {
        super(baseErrorCodeEnum, arguments);
    }
}