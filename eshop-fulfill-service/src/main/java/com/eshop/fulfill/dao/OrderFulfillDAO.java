package com.eshop.fulfill.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.fulfill.domain.entity.OrderFulfillDO;
import com.eshop.fulfill.mapper.OrderFulfillMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单履约表 DAO
 * @date 2022-04-08
 */
@Repository
public class OrderFulfillDAO extends BaseDAO<OrderFulfillMapper, OrderFulfillDO> {

    /**
     * 保存物流单号
     * @param fulfillId
     * @param logisticsCode
     */
    public boolean saveLogisticsCode(String fulfillId, String logisticsCode) {
        LambdaUpdateWrapper<OrderFulfillDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper
                .set(OrderFulfillDO::getLogisticsCode,logisticsCode)
                .eq(OrderFulfillDO::getFulfillId,fulfillId);
        return update(updateWrapper);
    }

    /**
     * 查询履约单
     * @param orderId
     * @return
     */
    public OrderFulfillDO getOne(String orderId) {
        LambdaQueryWrapper<OrderFulfillDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderFulfillDO::getOrderId,orderId);
        return baseMapper.selectOne(queryWrapper);
    }

    /**
     * 更新配送员信息
     * @return
     */
    public boolean updateDeliverer(String fulfillId,String delivererNo, String delivererName, String delivererPhone) {
        LambdaUpdateWrapper<OrderFulfillDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper
                .set(OrderFulfillDO::getDelivererNo,delivererNo)
                .set(OrderFulfillDO::getDelivererName,delivererName)
                .set(OrderFulfillDO::getDelivererPhone,delivererPhone)
                .eq(OrderFulfillDO::getFulfillId,fulfillId);
        return update(updateWrapper);
    }
}
