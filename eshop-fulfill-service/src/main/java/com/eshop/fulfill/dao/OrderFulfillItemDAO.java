package com.eshop.fulfill.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.fulfill.domain.entity.OrderFulfillItemDO;
import com.eshop.fulfill.mapper.OrderFulfillItemMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单履约条目表 DAO
 * @date 2022-04-08
 */
@Repository
public class OrderFulfillItemDAO extends BaseDAO<OrderFulfillItemMapper, OrderFulfillItemDO> {

    /**
     * 查询履约单item
     * @param fulfillId
     * @return
     */
    public List<OrderFulfillItemDO> listByFulfillId(String fulfillId) {
        LambdaQueryWrapper<OrderFulfillItemDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderFulfillItemDO::getFulfillId,fulfillId);
        return list(queryWrapper);
    }
}
