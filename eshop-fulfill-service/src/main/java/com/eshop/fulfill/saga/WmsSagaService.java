package com.eshop.fulfill.saga;

import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:  wms的saga service
 * @date 2022-04-08
 */
public interface WmsSagaService {

    /**
     * 捡货
     * @param request
     * @return
     */
    Boolean pickGoods(ReceiveFulfillRequest request);

    /**
     * 捡货补偿
     * @param request
     * @return
     */
    Boolean pickGoodsCompensate(ReceiveFulfillRequest request);
}

