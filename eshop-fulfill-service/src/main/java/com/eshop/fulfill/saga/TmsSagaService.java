package com.eshop.fulfill.saga;

import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-08
 */
public interface TmsSagaService {


    /**
     * 发货
     * @param request
     * @return
     */
    Boolean sendOut(ReceiveFulfillRequest request);

    /**
     * 发货补偿
     * @param request
     * @return
     */
    Boolean sendOutCompensate(ReceiveFulfillRequest request);
}
