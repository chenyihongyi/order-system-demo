package com.eshop.fulfill.saga.impl;

import com.alibaba.fastjson.JSONObject;
import com.eshop.common.core.JsonResult;
import com.eshop.common.utils.ObjectUtil;
import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;
import com.eshop.fulfill.exception.FulfillBizException;
import com.eshop.fulfill.exception.FulfillErrorCodeEnum;
import com.eshop.fulfill.saga.WmsSagaService;
import com.eshop.wms.api.WmsApi;
import com.eshop.wms.domain.PickDTO;
import com.eshop.wms.domain.PickGoodsRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-08
 */
@Service("wmsSageService")
@Slf4j
public class WmsSageServiceImpl implements WmsSagaService {

    @DubboReference(version = "1.0.0",retries = 0)
    private WmsApi wmsApi;

    @Override
    public Boolean pickGoods(ReceiveFulfillRequest request) {
        log.info("捡货，request={}", JSONObject.toJSONString(request));

        //调用wms系统进行捡货
        JsonResult<PickDTO> jsonResult = wmsApi
                .pickGoods(buildPickGoodsRequest(request));

        log.info("捡货结果，jsonResult={}", JSONObject.toJSONString(jsonResult));
        if(!jsonResult.getSuccess()) {
            throw new FulfillBizException(FulfillErrorCodeEnum.WMS_IS_ERROR);
        }

        return true;
    }

    @Override
    public Boolean pickGoodsCompensate(ReceiveFulfillRequest request) {

        log.info("补偿捡货，request={}", JSONObject.toJSONString(request));

        //调用wms系统进行捡货
        JsonResult<Boolean> jsonResult = wmsApi
                .cancelPickGoods(request.getOrderId());

        log.info("补偿捡货结果，jsonResult={}", JSONObject.toJSONString(jsonResult));
        if(!jsonResult.getSuccess()) {
            throw new FulfillBizException(FulfillErrorCodeEnum.WMS_IS_ERROR);
        }

        return true;
    }



    private PickGoodsRequest buildPickGoodsRequest(ReceiveFulfillRequest fulfillRequest) {
        PickGoodsRequest request = fulfillRequest.clone(PickGoodsRequest.class);
        List<PickGoodsRequest.OrderItemRequest> itemRequests = ObjectUtil
                .convertList(fulfillRequest.getReceiveOrderItems(), PickGoodsRequest.OrderItemRequest.class);
        request.setOrderItems(itemRequests);
        return request;
    }
}
