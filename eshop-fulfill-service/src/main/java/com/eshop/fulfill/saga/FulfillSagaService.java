package com.eshop.fulfill.saga;

import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: fulfull saga service
 * @date 2022-04-08
 */
public interface FulfillSagaService {

    /**
     * 创建履约单
     * @param request
     * @return
     */
    Boolean createFulfillOrder(ReceiveFulfillRequest request);


    /**
     * 补偿创建履约单
     * @param request
     * @return
     */
    Boolean createFulfillOrderCompensate(ReceiveFulfillRequest request);
}
