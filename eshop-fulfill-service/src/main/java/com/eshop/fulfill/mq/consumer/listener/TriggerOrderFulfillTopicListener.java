package com.eshop.fulfill.mq.consumer.listener;

import com.alibaba.fastjson.JSON;
import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;
import com.eshop.fulfill.service.FulfillService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description:接受订单履约消息
 * @date 2022-04-08
 */
@Slf4j
@Component
public class TriggerOrderFulfillTopicListener implements MessageListenerConcurrently {

    /**
     * 履约服务
     */
    @Autowired
    private FulfillService fulfillService;

    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list,
                                                    ConsumeConcurrentlyContext consumeConcurrentlyContext) {
        try {
            for(MessageExt messageExt : list) {
                String message = new String(messageExt.getBody());
                ReceiveFulfillRequest request =
                        JSON.parseObject(message, ReceiveFulfillRequest.class);
                fulfillService.receiveOrderFulFill(request);
            }
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        } catch (Exception e) {
            log.error("consumer error", e);
            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
        }
    }

}