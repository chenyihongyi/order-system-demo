package com.eshop.fulfill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-06
 */
@SpringBootApplication
public class FulfillApplication {

    public static void main(String[] args) {
        SpringApplication.run(FulfillApplication.class, args);
    }

}
