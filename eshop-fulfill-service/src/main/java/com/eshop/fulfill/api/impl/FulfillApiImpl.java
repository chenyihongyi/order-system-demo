package com.eshop.fulfill.api.impl;

import com.alibaba.fastjson.JSONObject;
import com.eshop.common.bean.SpringApplicationContext;
import com.eshop.common.constants.RocketMqConstant;
import com.eshop.common.core.JsonResult;
import com.eshop.common.enums.OrderStatusChangeEnum;
import com.eshop.fulfill.api.FulfillApi;
import com.eshop.fulfill.domain.request.CancelFulfillRequest;
import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;
import com.eshop.fulfill.domain.request.TriggerOrderWmsShipEventRequest;
import com.eshop.fulfill.exception.FulfillBizException;
import com.eshop.fulfill.mq.producer.DefaultProducer;
import com.eshop.fulfill.service.FulfillService;
import com.eshop.fulfill.service.OrderWmsShipEventProcessor;
import com.eshop.fulfill.service.impl.OrderDeliveredWmsEventProcessor;
import com.eshop.fulfill.service.impl.OrderOutStockWmsEventProcessor;
import com.eshop.fulfill.service.impl.OrderSignedWmsEventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-06
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = FulfillApi.class, retries = 0)
public class FulfillApiImpl implements FulfillApi {

    @Autowired
    private SpringApplicationContext springApplicationContext;

    @Autowired
    private DefaultProducer defaultProducer;

    @Autowired
    private FulfillService fulfillService;


    @Override
    public JsonResult<Boolean> receiveOrderFulFill(ReceiveFulfillRequest request) {
        try {
            Boolean result = fulfillService.receiveOrderFulFill(request);
            return JsonResult.buildSuccess(result);
        } catch (FulfillBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<Boolean> triggerOrderWmsShipEvent(TriggerOrderWmsShipEventRequest request) {
        log.info("触发订单物流配送结果事件，request={}", JSONObject.toJSONString(request));

        //1、获取处理器
        OrderStatusChangeEnum orderStatusChange = request.getOrderStatusChange();
        OrderWmsShipEventProcessor processor = getWmsShipEventProcessor(orderStatusChange);

        //2、执行
        if(null != processor) {
            processor.execute(request);
        }

        return JsonResult.buildSuccess(true);
    }


    @Override
    public JsonResult<Boolean> cancelFulfill(CancelFulfillRequest cancelFulfillRequest) {
        log.info("取消履约：request={}",JSONObject.toJSONString(cancelFulfillRequest));

        //发送取消履约消息
        defaultProducer.sendMessage(RocketMqConstant.CANCEL_FULFILL_TOPIC,
                JSONObject.toJSONString(cancelFulfillRequest), "取消履约");

        return JsonResult.buildSuccess(true);
    }

    /**
     * 订单物流配送结果处理器
     * @param orderStatusChange
     * @return
     */
    private OrderWmsShipEventProcessor getWmsShipEventProcessor(OrderStatusChangeEnum orderStatusChange) {
        if (OrderStatusChangeEnum.ORDER_OUT_STOCKED.equals(orderStatusChange)) {
            //订单已出库事件
            return springApplicationContext.getBean(OrderOutStockWmsEventProcessor.class);
        } else if (OrderStatusChangeEnum.ORDER_DELIVERED.equals(orderStatusChange)) {
            //订单已配送事件
            return springApplicationContext.getBean(OrderDeliveredWmsEventProcessor.class);
        } else if (OrderStatusChangeEnum.ORDER_SIGNED.equals(orderStatusChange)) {
            //订单已签收事件
            return springApplicationContext.getBean(OrderSignedWmsEventProcessor.class);
        }
        return null;
    }

}
