package com.eshop.fulfill.builder;

import com.eshop.common.utils.ObjectUtil;
import com.eshop.fulfill.domain.entity.OrderFulfillDO;
import com.eshop.fulfill.domain.entity.OrderFulfillItemDO;
import com.eshop.fulfill.domain.request.ReceiveFulfillRequest;
import lombok.Data;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单履约数据构造器
 * @date 2022-04-08
 */
@Data
public class FulfillDataBuilder {

    /**
     * 订单履约
     */
    private OrderFulfillDO orderFulFill;

    /**
     * 订单履约条目
     */
    private List<OrderFulfillItemDO> orderFulFillItems;

    /**
     * 接受订单履约请求
     */
    private ReceiveFulfillRequest receiveFulFillRequest;


    public FulfillDataBuilder(ReceiveFulfillRequest receiveFulFillRequest) {
        this.receiveFulFillRequest = receiveFulFillRequest;
    }

    public static FulfillDataBuilder builder(ReceiveFulfillRequest receiveFulfillRequest) {
        return new FulfillDataBuilder(receiveFulfillRequest);
    }

    public FulfillDataBuilder buildOrderFulfill(String fulfillId) {
        orderFulFill = receiveFulFillRequest.clone(OrderFulfillDO.class);
        orderFulFill.setFulfillId(fulfillId);
        return this;
    }

    public FulfillDataBuilder buildOrderFulfillItem() {
        orderFulFillItems = ObjectUtil
                .convertList(receiveFulFillRequest.getReceiveOrderItems(), OrderFulfillItemDO.class);

        //设置履约单ID
        for(OrderFulfillItemDO item : orderFulFillItems) {
            item.setFulfillId(orderFulFill.getFulfillId());
        }

        return this;
    }

}
