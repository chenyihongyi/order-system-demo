package com.eshop.product.api;

import com.eshop.common.core.JsonResult;
import com.eshop.product.domain.dto.ProductSkuDTO;
import com.eshop.product.domain.query.ProductSkuQuery;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface ProductApi {

    /**
     * 查询商品SKU详情
     * @param productSkuQuery
     * @return
     */
    JsonResult<ProductSkuDTO> getProductSku(ProductSkuQuery productSkuQuery);

}
