package com.eshop.product.domain.query;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@Data
public class ProductSkuQuery extends AbstractObject implements Serializable {

    private static final long serialVersionUID = 4788741095015777932L;

    /**
     * 卖家ID
     */
    private String sellerId;

    /**
     * 商品skuCode
     */
    private String skuCode;
}
