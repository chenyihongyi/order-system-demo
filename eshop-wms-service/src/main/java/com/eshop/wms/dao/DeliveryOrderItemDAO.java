package com.eshop.wms.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.wms.domain.entity.DeliveryOrderItemDO;
import com.eshop.wms.mapper.DeliveryOrderItemMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 出库单条目 DAO
 * @date 2022-04-08
 */
@Repository
public class DeliveryOrderItemDAO extends BaseDAO<DeliveryOrderItemMapper, DeliveryOrderItemDO> {

    /**
     * 查询出库单item
     * @param deliveryOrderId
     * @return
     */
    public List<DeliveryOrderItemDO> listByDeliveryOrderId(String deliveryOrderId) {
        LambdaQueryWrapper<DeliveryOrderItemDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DeliveryOrderItemDO::getDeliveryOrderId,deliveryOrderId);
        return list(queryWrapper);
    }

}
