package com.eshop.wms.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.wms.domain.entity.DeliveryOrderDO;
import com.eshop.wms.mapper.DeliveryOrderMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 出库单 DAO
 * @date 2022-04-08
 */
@Repository
public class DeliveryOrderDAO extends BaseDAO<DeliveryOrderMapper, DeliveryOrderDO> {

    /**
     * 查询出库单
     * @param orderId
     * @return
     */
    public List<DeliveryOrderDO> listByOrderId(String orderId) {
        LambdaQueryWrapper<DeliveryOrderDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DeliveryOrderDO::getOrderId,orderId);
        return list(queryWrapper);
    }

}
