package com.eshop.wms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.wms.domain.entity.DeliveryOrderItemDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 出库单条目 Mapper 接口
 * @date 2022-04-08
 */
@Mapper
public interface DeliveryOrderItemMapper extends BaseMapper<DeliveryOrderItemDO> {

}
