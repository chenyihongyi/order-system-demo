package com.eshop.wms.exception;

import com.eshop.common.exception.BaseBizException;
import com.eshop.common.exception.BaseErrorCodeEnum;

/**
 * @author Elvis
 * @version 1.0
 * @description: wms中心自定义业务异常类
 * @date 2022-04-08
 */
public class WmsBizException extends BaseBizException {

    public WmsBizException(String errorMsg) {
        super(errorMsg);
    }

    public WmsBizException(String errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public WmsBizException(BaseErrorCodeEnum baseErrorCodeEnum) {
        super(baseErrorCodeEnum);
    }

    public WmsBizException(String errorCode, String errorMsg, Object... arguments) {
        super(errorCode, errorMsg, arguments);
    }

    public WmsBizException(BaseErrorCodeEnum baseErrorCodeEnum, Object... arguments) {
        super(baseErrorCodeEnum, arguments);
    }
}
