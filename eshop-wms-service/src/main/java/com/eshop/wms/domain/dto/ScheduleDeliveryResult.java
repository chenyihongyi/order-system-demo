package com.eshop.wms.domain.dto;

import com.eshop.wms.domain.entity.DeliveryOrderDO;
import com.eshop.wms.domain.entity.DeliveryOrderItemDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 调度出库结果
 * @date 2022-04-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDeliveryResult {

    /**
     * 调度出库单
     */
    private DeliveryOrderDO deliveryOrder;

    /**
     * 调度出库单条目
     */
    private List<DeliveryOrderItemDO> deliveryOrderItems;
}
