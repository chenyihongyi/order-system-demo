package com.eshop.wms.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description:捡货结果DTO
 * @date 2022-04-08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PickDTO implements Serializable {
    /**
     * 订单ID
     */
    private String orderId;
}
