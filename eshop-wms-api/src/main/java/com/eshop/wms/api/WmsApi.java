package com.eshop.wms.api;

import com.eshop.common.core.JsonResult;
import com.eshop.wms.domain.PickDTO;
import com.eshop.wms.domain.PickGoodsRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 仓储系统api
 * @date 2022-04-08
 */
public interface WmsApi {

    /**
     * 捡货
     * @param request
     * @return
     */
    JsonResult<PickDTO> pickGoods(PickGoodsRequest request);

    /**
     * 取消捡货
     * @param orderId
     * @return
     */
    JsonResult<Boolean> cancelPickGoods(String orderId);

}
