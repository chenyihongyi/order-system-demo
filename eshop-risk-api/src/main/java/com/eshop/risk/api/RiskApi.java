package com.eshop.risk.api;

import com.eshop.common.core.JsonResult;
import com.eshop.risk.domain.dto.CheckOrderRiskDTO;
import com.eshop.risk.domain.CheckOrderRiskRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 风控服务API
 * @date 2022-04-05
 */
public interface RiskApi {

    /**
     * 订单风控检查
     * @param checkOrderRiskRequest
     * @return
     */
    JsonResult<CheckOrderRiskDTO> checkOrderRisk(CheckOrderRiskRequest checkOrderRiskRequest);

}
