package com.eshop.risk.domain;

import com.eshop.common.core.AbstractObject;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单风控检查入参
 * @date 2022-04-05
 */
@Data
public class CheckOrderRiskRequest extends AbstractObject implements Serializable {

    private static final long serialVersionUID = 122317037326617884L;

    /**
     * 业务线标识
     */
    private Integer businessIdentifier;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 卖家ID
     */
    private String sellerId;

    /**
     * 客户端ip
     */
    private String clientIp;

    /**
     * 设备标识
     */
    private String deviceId;

}
