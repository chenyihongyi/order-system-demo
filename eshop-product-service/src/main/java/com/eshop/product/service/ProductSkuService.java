package com.eshop.product.service;

import com.eshop.product.domain.dto.ProductSkuDTO;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface ProductSkuService {


    /**
     * 根据skuCode获取商品sku信息
     * @param skuCode
     * @return
     */
    ProductSkuDTO getProductSkuByCode(String skuCode);
}
