package com.eshop.product.service.impl;

import com.eshop.common.utils.ParamCheckUtil;
import com.eshop.product.dao.ProductSkuDAO;
import com.eshop.product.domain.dto.ProductSkuDTO;
import com.eshop.product.domain.entity.ProductSkuDO;
import com.eshop.product.exception.ProductErrorCodeEnum;
import com.eshop.product.service.ProductSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@Service
public class ProductSkuServiceImpl implements ProductSkuService {

    @Autowired
    private ProductSkuDAO productSkuDAO;

    @Override
    public ProductSkuDTO getProductSkuByCode(String skuCode) {
        ParamCheckUtil.checkStringNonEmpty(skuCode, ProductErrorCodeEnum.SKU_CODE_IS_NULL);

        ProductSkuDO productSkuDO = productSkuDAO.getProductSkuByCode(skuCode);
        if(productSkuDO == null) {
            return null;
        }
        return productSkuDO.clone(ProductSkuDTO.class);
    }
}
