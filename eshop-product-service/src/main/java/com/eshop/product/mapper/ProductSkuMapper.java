package com.eshop.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.product.domain.entity.ProductSkuDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 商品sku记录表 Mapper 接口
 * @date 2022-04-05
 */
@Mapper
public interface ProductSkuMapper extends BaseMapper<ProductSkuDO> {



}
