package com.eshop.product.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.product.domain.entity.ProductSkuDO;
import com.eshop.product.mapper.ProductSkuMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 商品sku记录表 Mapper 接口
 * @date 2022-04-05
 */
@Repository
public class ProductSkuDAO extends BaseDAO<ProductSkuMapper, ProductSkuDO> {

    /**
     * 根据skuCode获取商品信息
     * @param skuCode
     * @return
     */
    public ProductSkuDO getProductSkuByCode(String skuCode) {
        QueryWrapper<ProductSkuDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sku_code", skuCode);
        return getOne(queryWrapper);
    }

}
