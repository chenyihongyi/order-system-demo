package com.eshop.product.api.impl;

import com.alibaba.fastjson.JSONObject;
import com.eshop.common.core.JsonResult;
import com.eshop.common.utils.ParamCheckUtil;
import com.eshop.product.api.ProductApi;
import com.eshop.product.domain.dto.ProductSkuDTO;
import com.eshop.product.domain.query.ProductSkuQuery;
import com.eshop.product.exception.ProductBizException;
import com.eshop.product.service.ProductSkuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Elvis
 * @version 1.0
 * @description: 商品中心-商品信息
 * @date 2022-04-05
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = ProductApi.class)
public class ProductApiImpl implements ProductApi {

    @Autowired
    private ProductSkuService productSkuService;

    @Override
    public JsonResult<ProductSkuDTO> getProductSku(ProductSkuQuery productSkuQuery) {
        try {
            ParamCheckUtil.checkObjectNonNull(productSkuQuery);
            String skuCode = productSkuQuery.getSkuCode();

            ProductSkuDTO productSkuDTO = productSkuService.getProductSkuByCode(skuCode);
            log.info("productSkuDTO={},productSkuQuery={}"
                    , JSONObject.toJSONString(productSkuDTO),JSONObject.toJSONString(productSkuQuery));
            return JsonResult.buildSuccess(productSkuDTO);
        } catch(ProductBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch(Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
