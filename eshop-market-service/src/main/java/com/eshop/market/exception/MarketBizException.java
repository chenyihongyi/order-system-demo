package com.eshop.market.exception;

import com.eshop.common.exception.BaseBizException;
import com.eshop.common.exception.BaseErrorCodeEnum;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public class MarketBizException extends BaseBizException {

    public MarketBizException(String errorMsg) {
        super(errorMsg);
    }

    public MarketBizException(String errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public MarketBizException(BaseErrorCodeEnum baseErrorCodeEnum) {
        super(baseErrorCodeEnum);
    }

    public MarketBizException(String errorCode, String errorMsg, Object... arguments) {
        super(errorCode, errorMsg, arguments);
    }

    public MarketBizException(BaseErrorCodeEnum baseErrorCodeEnum, Object... arguments) {
        super(baseErrorCodeEnum, arguments);
    }
}