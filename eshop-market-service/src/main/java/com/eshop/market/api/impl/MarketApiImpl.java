package com.eshop.market.api.impl;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */

import com.eshop.common.core.JsonResult;
import com.eshop.market.api.MarketApi;
import com.eshop.market.domain.dto.CalculateOrderAmountDTO;
import com.eshop.market.domain.dto.UserCouponDTO;
import com.eshop.market.domain.query.UserCouponQuery;
import com.eshop.market.domain.request.CalculateOrderAmountRequest;
import com.eshop.market.domain.request.LockUserCouponRequest;
import com.eshop.market.exception.MarketBizException;
import com.eshop.market.service.CouponService;
import com.eshop.market.service.MarketService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Elvis
 * @version 1.0
 * @description: 计算订单费用
 * @date 2022-04-05
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = MarketApi.class, retries = 0)
public class MarketApiImpl implements MarketApi {

        @Autowired
        private CouponService couponService;

        @Autowired
        private MarketService marketService;

        @Override
        public JsonResult<UserCouponDTO> getUserCoupon(UserCouponQuery userCouponQuery) {
                try {
                        UserCouponDTO userCouponDTO = couponService.getUserCoupon(userCouponQuery);
                        return JsonResult.buildSuccess(userCouponDTO);
                } catch (MarketBizException e) {
                        log.error("biz error", e);
                        return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
                } catch (Exception e) {
                        log.error("system error", e);
                        return JsonResult.buildError(e.getMessage());
                }
        }

        /**
         * 计算订单费用
         *
         * @param calculateOrderAmountRequest
         * @return
         */
        @Override
        public JsonResult<CalculateOrderAmountDTO> calculateOrderAmount(CalculateOrderAmountRequest calculateOrderAmountRequest) {
                try {
                        CalculateOrderAmountDTO calculateOrderAmountDTO = marketService.calculateOrderAmount(calculateOrderAmountRequest);
                        return JsonResult.buildSuccess(calculateOrderAmountDTO);
                } catch (MarketBizException e) {
                        log.error("biz error", e);
                        return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
                } catch (Exception e) {
                        log.error("system error", e);
                        return JsonResult.buildError(e.getMessage());
                }
        }

        @Override
        public JsonResult<Boolean> lockUserCoupon(LockUserCouponRequest lockUserCouponRequest) {
                try {
                        Boolean result = couponService.lockUserCoupon(lockUserCouponRequest);
                        return JsonResult.buildSuccess(result);
                } catch (MarketBizException e) {
                        log.error("biz error", e);
                        return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
                } catch (Exception e) {
                        log.error("system error", e);
                        return JsonResult.buildError(e.getMessage());
                }
        }
}