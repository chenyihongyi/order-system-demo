package com.eshop.market.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.market.domain.entity.FreightTemplateDO;
import com.eshop.market.mapper.FreightTemplateMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板 DAO 接口
 * @date 2022-04-05
 */
@Repository
public class FreightTemplateDAO extends BaseDAO<FreightTemplateMapper, FreightTemplateDO> {

    @Autowired
    private FreightTemplateMapper freightTemplateMapper;

    /**
     * 通过区域ID查找运费模板
     */
    public FreightTemplateDO getByRegionId(String regionId) {
        QueryWrapper<FreightTemplateDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("region_id", regionId);
        return freightTemplateMapper.selectOne(queryWrapper);
    }

}
