package com.eshop.market.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.market.domain.entity.CouponConfigDO;
import com.eshop.market.mapper.CouponConfigMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 优惠券管理DAO组件
 * @date 2022-04-05
 */
@Repository
public class CouponConfigDAO extends BaseDAO<CouponConfigMapper, CouponConfigDO> {

    /**
     * 优惠券配置信息
     * @param couponConfigId
     * @return
     */
    public CouponConfigDO getByCouponConfigId(String couponConfigId) {
        QueryWrapper<CouponConfigDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("coupon_config_id", couponConfigId);
        return getOne(queryWrapper);
    }

}
