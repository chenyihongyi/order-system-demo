package com.eshop.market.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.market.domain.entity.CouponDO;
import com.eshop.market.mapper.CouponMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 优惠券领取记录管理DAO组件
 * @date 2022-04-05
 */
@Repository
public class CouponDAO extends BaseDAO<CouponMapper, CouponDO> {

    /**
     * 查询优惠券
     * @param userId
     * @param couponId
     * @return
     */
    public CouponDO getUserCoupon(String userId, String couponId) {
        QueryWrapper<CouponDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId)
                .eq("coupon_id", couponId);
        return getOne(queryWrapper);
    }
}
