package com.eshop.market.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.eshop.common.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板
 * @date 2022-04-05
 */
@Data
@TableName("market_freight_template")
public class FreightTemplateDO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 模板名称
     */
    private String name;

    /**
     * 区域ID
     */
    private String regionId;

    /**
     * 标准运费
     */
    private Integer shippingAmount;

    /**
     * 订单满多少钱则免运费
     */
    private Integer conditionAmount;

}
