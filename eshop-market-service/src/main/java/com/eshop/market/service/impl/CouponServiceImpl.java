package com.eshop.market.service.impl;

import com.eshop.common.utils.ParamCheckUtil;
import com.eshop.market.dao.CouponConfigDAO;
import com.eshop.market.dao.CouponDAO;
import com.eshop.market.domain.dto.UserCouponDTO;
import com.eshop.market.domain.entity.CouponConfigDO;
import com.eshop.market.domain.entity.CouponDO;
import com.eshop.market.domain.query.UserCouponQuery;
import com.eshop.market.domain.request.LockUserCouponRequest;
import com.eshop.market.enums.CouponUsedStatusEnum;
import com.eshop.market.exception.MarketBizException;
import com.eshop.market.exception.MarketErrorCodeEnum;
import com.eshop.market.service.CouponService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author Elvis
 * @version 1.0
 * @description: 优惠券管理service组件
 * @date 2022-04-05
 */
@Service
@Slf4j
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponConfigDAO couponConfigDAO;

    @Autowired
    private CouponDAO couponDAO;

    /**
     * 锁定用户优惠券
     *
     * @param lockUserCouponRequest
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean lockUserCoupon(LockUserCouponRequest lockUserCouponRequest) {
        // 检查入参
        checkLockUserCouponRequest(lockUserCouponRequest);

        String userId = lockUserCouponRequest.getUserId();
        String couponId = lockUserCouponRequest.getCouponId();
        CouponDO couponDO = couponDAO.getUserCoupon(userId, couponId);
        if (couponDO == null) {
            throw new MarketBizException(MarketErrorCodeEnum.USER_COUPON_IS_NULL);
        }
        // 判断优惠券是否已经使用了
        if (CouponUsedStatusEnum.USED.getCode().equals(couponDO.getUsed())) {
            throw new MarketBizException(MarketErrorCodeEnum.USER_COUPON_IS_USED);
        }
        couponDO.setUsed(CouponUsedStatusEnum.USED.getCode());
        couponDO.setUsedTime(new Date());
        couponDAO.updateById(couponDO);
        return true;
    }

    /**
     * 锁定用户优惠券入参检查
     *
     * @param lockUserCouponRequest
     */
    private void checkLockUserCouponRequest(LockUserCouponRequest lockUserCouponRequest) {
        String userId = lockUserCouponRequest.getUserId();
        String couponId = lockUserCouponRequest.getCouponId();
        ParamCheckUtil.checkStringNonEmpty(userId);
        ParamCheckUtil.checkStringNonEmpty(couponId);
    }

    @Override
    public UserCouponDTO getUserCoupon(UserCouponQuery userCouponQuery) {
        // 入参检查
        String userId = userCouponQuery.getUserId();
        String couponId = userCouponQuery.getCouponId();
        ParamCheckUtil.checkStringNonEmpty(userId);
        ParamCheckUtil.checkStringNonEmpty(couponId);

        // 判断用户优惠券是否存在
        CouponDO couponDO = couponDAO.getUserCoupon(userId, couponId);
        if(couponDO == null) {
            throw new MarketBizException(MarketErrorCodeEnum.USER_COUPON_IS_NULL);
        }
        String couponConfigId = couponDO.getCouponConfigId();

        // 判断优惠券活动配置信息是否存在
        CouponConfigDO couponConfigDO = couponConfigDAO.getByCouponConfigId(couponConfigId);
        if(couponConfigDO == null) {
            throw new MarketBizException(MarketErrorCodeEnum.USER_COUPON_CONFIG_IS_NULL);
        }
        UserCouponDTO userCouponDTO = new UserCouponDTO();
        userCouponDTO.setUserId(userId);
        userCouponDTO.setCouponConfigId(couponConfigId);
        userCouponDTO.setCouponId(couponId);
        userCouponDTO.setName(couponConfigDO.getName());
        userCouponDTO.setType(couponConfigDO.getType());
        userCouponDTO.setAmount(couponConfigDO.getAmount());
        userCouponDTO.setConditionAmount(couponConfigDO.getConditionAmount());
        userCouponDTO.setValidStartTime(couponConfigDO.getValidStartTime());
        userCouponDTO.setValidEndTime(couponConfigDO.getValidEndTime());
        return userCouponDTO;
    }

}
