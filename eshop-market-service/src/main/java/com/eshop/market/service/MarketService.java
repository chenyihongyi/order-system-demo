package com.eshop.market.service;

import com.eshop.market.domain.dto.CalculateOrderAmountDTO;
import com.eshop.market.domain.request.CalculateOrderAmountRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 营销管理service接口
 * @date 2022-04-05
 */
public interface MarketService {

    /**
     * 计算订单费用
     *
     * @param calculateOrderAmountRequest
     * @return
     */
    CalculateOrderAmountDTO calculateOrderAmount(CalculateOrderAmountRequest calculateOrderAmountRequest);

}
