package com.eshop.market.service;

import com.eshop.market.domain.dto.UserCouponDTO;
import com.eshop.market.domain.query.UserCouponQuery;
import com.eshop.market.domain.request.LockUserCouponRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description: 优惠券管理service接口
 * @date 2022-04-05
 */
public interface CouponService {

    /**
     * 锁定用户优惠券
     *
     * @param lockUserCouponRequest
     * @return
     */
    Boolean lockUserCoupon(LockUserCouponRequest lockUserCouponRequest);

    /**
     * 查询用户的优惠券信息
     * @param userCouponQuery
     * @return
     */
    UserCouponDTO getUserCoupon(UserCouponQuery userCouponQuery);
}
