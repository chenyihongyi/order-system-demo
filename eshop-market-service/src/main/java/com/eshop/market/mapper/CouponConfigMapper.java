package com.eshop.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.market.domain.entity.CouponConfigDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 优惠券配置表 Mapper 接口
 * @date 2022-04-05
 */
@Mapper
public interface CouponConfigMapper extends BaseMapper<CouponConfigDO> {

}
