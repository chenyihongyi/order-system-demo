package com.eshop.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.market.domain.entity.FreightTemplateDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 运费模板 Mapper 接口
 * @date 2022-04-05
 */
@Mapper
public interface FreightTemplateMapper extends BaseMapper<FreightTemplateDO> {

}
