package com.eshop.market.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.market.domain.entity.CouponDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 用户优惠券记录表 Mapper 接口
 * @date 2022-04-05
 */
@Mapper
public interface CouponMapper extends BaseMapper<CouponDO> {

}