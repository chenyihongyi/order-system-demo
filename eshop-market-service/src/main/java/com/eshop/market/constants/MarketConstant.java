package com.eshop.market.constants;

/**
 * @author Elvis
 * @version 1.0
 * @description: 营销中心常量类
 * @date 2022-04-05
 */
public class MarketConstant {

    /**
     * 默认的标准运费 5元
     */
    public static Integer DEFAULT_SHIPPING_AMOUNT = 500;

    /**
     * 默认情况下满49元免运费
     */
    public static Integer DEFAULT_CONDITION_AMOUNT = 4900;



}
