package com.eshop.customer.api.impl;

import com.eshop.common.core.JsonResult;
import com.eshop.customer.api.CustomerApi;
import com.eshop.customer.domain.request.CustomerReviewReturnGoodsRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-06
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = CustomerApi.class, retries = 0)
public class CustomerApiImpl implements CustomerApi {

    @Override
    public JsonResult<Boolean> customerAudit(CustomerReviewReturnGoodsRequest customerReviewReturnGoodsRequest) {
        log.info("收到提交的审核申请");
        return JsonResult.buildSuccess(true);
    }
}