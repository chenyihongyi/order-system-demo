package com.eshop.order.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.eshop.common.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单编号表
 * @date 2022-04-05
 */
@Data
@TableName("order_auto_no")
public class OrderAutoNoDO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

}
