package com.eshop.order.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.eshop.common.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单快照表
 * @date 2022-04-05
 */
@Data
@TableName("order_snapshot")
public class OrderSnapshotDO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 快照类型
     */
    private Integer snapshotType;

    /**
     * 订单快照内容
     */
    private String snapshotJson;

}

