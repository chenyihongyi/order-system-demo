package com.eshop.order.domain.dto;

import com.eshop.order.domain.entity.OrderInfoDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 校验缺品请求结果DTO
 * @date 2022-04-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckLackDTO {

    /**
     * 订单
     */
    private OrderInfoDO order;

    /**
     * 缺品订单item
     */
    private List<LackItemDTO> lackItems;

}
