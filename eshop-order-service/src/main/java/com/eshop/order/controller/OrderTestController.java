package com.eshop.order.controller;

import com.alibaba.fastjson.JSONObject;
import com.eshop.common.enums.OrderStatusChangeEnum;
import com.eshop.fulfill.api.FulfillApi;
import com.eshop.fulfill.domain.event.OrderOutStockWmsEvent;
import com.eshop.order.api.OrderApi;
import com.eshop.order.api.OrderQueryApi;
import com.eshop.common.core.JsonResult;
import com.eshop.order.domain.dto.CreateOrderDTO;
import com.eshop.order.domain.dto.GenOrderIdDTO;
import com.eshop.order.domain.dto.PrePayOrderDTO;
import com.eshop.order.domain.request.CreateOrderRequest;
import com.eshop.order.domain.request.GenOrderIdRequest;
import com.eshop.order.domain.request.PayCallbackRequest;
import com.eshop.order.domain.request.PrePayOrderRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Elvis
 * @version 1.0
 * @description: 正向下单流程接口冒烟测试
 * @date 2022-04-05
 */
@Api(tags = "订单：正向下单流程接口冒烟测试")
@RestController
@Slf4j
@RequestMapping("/order/test")
public class OrderTestController {

    /**
     * 订单服务
     */
    @DubboReference(version = "1.0.0", retries = 0)
    private OrderApi orderApi;

    @DubboReference(version = "1.0.0")
    private OrderQueryApi queryApi;

    @DubboReference(version = "1.0.0", retries = 0)
    private FulfillApi fulfillApi;

    /**
     * 测试生成新的订单号
     * @param genOrderIdRequest
     * @return
     */
    @PostMapping("/genOrderId")
    @ApiOperation(value = "测试生成新的订单号", notes = "测试生成新的订单号")
    public JsonResult<GenOrderIdDTO> genOrderId(@RequestBody GenOrderIdRequest genOrderIdRequest) {
        JsonResult<GenOrderIdDTO> genOrderIdDTO = orderApi.genOrderId(genOrderIdRequest);
        return genOrderIdDTO;
    }

    /**
     * 测试提交订单
     * @param createOrderRequest
     * @return
     */
    @PostMapping("/createOrder")
    @ApiOperation(value = "测试提交订单", notes = "测试提交订单")
    public JsonResult<CreateOrderDTO> createOrder(@RequestBody CreateOrderRequest createOrderRequest) {
        JsonResult<CreateOrderDTO> createOrderDTO = orderApi.createOrder(createOrderRequest);
        return createOrderDTO;
    }

    /**
     * 测试预支付订单
     * @param prePayOrderRequest
     * @return
     */
    @PostMapping("/prePayOrder")
    @ApiOperation(value = "测试预支付订单", notes = "测试预支付订单")
    public JsonResult<PrePayOrderDTO> prePayOrder(@RequestBody PrePayOrderRequest prePayOrderRequest) {
        JsonResult<PrePayOrderDTO> prePayOrderDTO = orderApi.prePayOrder(prePayOrderRequest);
        return prePayOrderDTO;
    }

    /**
     * 测试支付回调
     * @param payCallbackRequest
     * @return
     */
    @PostMapping("/payCallback")
    @ApiOperation(value = "测试支付回调", notes = "测试支付回调")
    public JsonResult<Boolean> payCallback(@RequestBody PayCallbackRequest payCallbackRequest) {
        JsonResult<Boolean> result = orderApi.payCallback(payCallbackRequest);
        return result;
    }

    /**
     * 触发订单发货出库事件
     * @param event
     * @return
     */
    @PostMapping("/triggerOutStockEvent")
    @ApiOperation(value = "触发订单发货出库事件", notes = "触发订单发货出库事件")
    public JsonResult<Boolean> triggerOrderOutStockWmsEvent(@RequestBody OrderOutStockWmsEvent event) {
        log.info("orderId={},event={}",event.getOrderId(), JSONObject.toJSONString(event));
        JsonResult<Boolean> result = fulfillApi.triggerOrderWmsShipEvent(event.getOrderId()
                , OrderStatusChangeEnum.ORDER_OUT_STOCKED,event);
        return result;
    }
}
