package com.eshop.order.mq.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Elvis
 * @version 1.0
 * @description:rocketmq的配置信息
 * @date 2022-04-06
 */
@Configuration
@EnableConfigurationProperties(RocketMQProperties.class)
public class RocketMQConfig {

}