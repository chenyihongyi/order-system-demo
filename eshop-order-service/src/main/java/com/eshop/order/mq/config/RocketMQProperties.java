package com.eshop.order.mq.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Elvis
 * @version 1.0
 * @description:rocketmq的配置信息
 * @date 2022-04-06
 */
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMQProperties {
	
    private String nameServer;

	public String getNameServer() {
		return nameServer;
	}

	public void setNameServer(String nameServer) {
		this.nameServer = nameServer;
	}
}