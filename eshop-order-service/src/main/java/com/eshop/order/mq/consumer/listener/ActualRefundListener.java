package com.eshop.order.mq.consumer.listener;

import com.alibaba.fastjson.JSONObject;
import com.eshop.common.core.JsonResult;
import com.eshop.common.message.ActualRefundMessage;
import com.eshop.order.exception.OrderBizException;
import com.eshop.order.service.OrderAfterSaleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-06
 */
@Slf4j
@Component
public class ActualRefundListener implements MessageListenerConcurrently {

    @Autowired
    private OrderAfterSaleService orderAfterSaleService;

    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
        try {
            for(MessageExt messageExt : list) {
                String message = new String(messageExt.getBody());
                ActualRefundMessage actualRefundMessage = JSONObject.parseObject(message, ActualRefundMessage.class);
                log.info("ActualRefundConsumer message:{}", message);

                JsonResult<Boolean> jsonResult = orderAfterSaleService.refundMoney(actualRefundMessage);
                if (!jsonResult.getSuccess()) {
                    throw new OrderBizException(jsonResult.getErrorCode(), jsonResult.getErrorMessage());
                }
            }
            return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
        } catch (Exception e) {
            log.error("consumer error", e);
            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
        }
    }
}
