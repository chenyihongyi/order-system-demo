package com.eshop.order.wms;

import com.eshop.order.domain.dto.WmsShipDTO;
import com.eshop.order.exception.OrderBizException;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单物流配送结果处理器
 * @date 2022-04-06
 */
public interface OrderWmsShipResultProcessor {

    /**
     * 执行具体的业务逻辑
     * @throws OrderBizException
     */
    void execute(WmsShipDTO wmsShipDTO) throws OrderBizException;
}
