package com.eshop.order.manager;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单号生成manager组件
 * @date 2022-04-05
 */
public interface OrderNoManager {

    /**
     * 生成订单号
     * @param orderNoType 订单号类型
     * @param userId 用户ID
     * @return
     */
    String genOrderId(Integer orderNoType, String userId);
}
