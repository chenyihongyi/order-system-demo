package com.eshop.order.api.impl;

import com.eshop.common.core.JsonResult;
import com.eshop.order.api.OrderApi;
import com.eshop.order.domain.dto.CreateOrderDTO;
import com.eshop.order.domain.dto.GenOrderIdDTO;
import com.eshop.order.domain.dto.PrePayOrderDTO;
import com.eshop.order.domain.request.CreateOrderRequest;
import com.eshop.order.domain.request.GenOrderIdRequest;
import com.eshop.order.domain.request.PayCallbackRequest;
import com.eshop.order.domain.request.PrePayOrderRequest;
import com.eshop.order.exception.OrderBizException;
import com.eshop.order.exception.OrderErrorCodeEnum;
import com.eshop.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单中心接口
 * @date 2022-04-05
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = OrderApi.class, retries = 0)
public class OrderApiImpl implements OrderApi {

    @Autowired
    private OrderService orderService;

    /**
     * 生成订单号接口
     *
     * @param genOrderIdRequest 生成订单号入参
     * @return 订单号
     */
    @Override
    public JsonResult<GenOrderIdDTO> genOrderId(GenOrderIdRequest genOrderIdRequest) {
        try {
            String userId = genOrderIdRequest.getUserId();
            if (userId == null || "".equals(userId)) {
                return JsonResult.buildError(OrderErrorCodeEnum.USER_ID_IS_NULL);
            }
            GenOrderIdDTO genOrderIdDTO = orderService.genOrderId(genOrderIdRequest);
            return JsonResult.buildSuccess(genOrderIdDTO);
        } catch (OrderBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 提交订单/生成订单接口
     *
     * @param createOrderRequest 提交订单请求入参
     * @return 订单号
     */
    @Override
    public JsonResult<CreateOrderDTO> createOrder(CreateOrderRequest createOrderRequest) {
        try {
            CreateOrderDTO createOrderDTO = orderService.createOrder(createOrderRequest);
            return JsonResult.buildSuccess(createOrderDTO);
        } catch (OrderBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 预支付订单接口
     *
     * @param prePayOrderRequest 预支付订单请求入参
     * @return
     */
    @Override
    public JsonResult<PrePayOrderDTO> prePayOrder(PrePayOrderRequest prePayOrderRequest) {
        try {
            PrePayOrderDTO prePayOrderDTO = orderService.prePayOrder(prePayOrderRequest);
            return JsonResult.buildSuccess(prePayOrderDTO);
        } catch (OrderBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }

    }

    /**
     * 支付回调接口
     *
     * @param payCallbackRequest 支付系统回调入参
     * @return
     */
    @Override
    public JsonResult<Boolean> payCallback(PayCallbackRequest payCallbackRequest) {
        try {
            orderService.payCallback(payCallbackRequest);
            return JsonResult.buildSuccess(true);
        } catch (OrderBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
