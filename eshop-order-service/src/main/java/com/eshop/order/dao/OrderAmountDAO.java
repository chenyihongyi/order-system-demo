package com.eshop.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.order.domain.entity.OrderAmountDO;
import com.eshop.order.mapper.OrderAmountMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单价格表 DAO
 * @date 2022-04-05
 */
@Repository
public class OrderAmountDAO extends BaseDAO<OrderAmountMapper, OrderAmountDO> {

    /**
     * 根据订单号查询订单价格
     * @param orderId
     * @return
     */
    public List<OrderAmountDO> listByOrderId(String orderId) {
        LambdaQueryWrapper<OrderAmountDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderAmountDO::getOrderId,orderId);
        return list(queryWrapper);
    }

    /**
     * 查询订单指定类型费用
     * @param orderId
     * @param amountType
     * @return
     */
    public OrderAmountDO getOne(String orderId,Integer amountType) {
        LambdaQueryWrapper<OrderAmountDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderAmountDO::getOrderId,orderId)
                .eq(OrderAmountDO::getAmountType,amountType);
        return baseMapper.selectOne(queryWrapper);
    }
}
