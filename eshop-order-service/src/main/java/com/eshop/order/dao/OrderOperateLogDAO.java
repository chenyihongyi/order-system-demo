package com.eshop.order.dao;

import com.eshop.common.dao.BaseDAO;
import com.eshop.order.domain.entity.OrderOperateLogDO;
import com.eshop.order.mapper.OrderOperateLogMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单操作日志表 DAO
 * @date 2022-04-05
 */
@Repository
public class OrderOperateLogDAO extends BaseDAO<OrderOperateLogMapper, OrderOperateLogDO> {
}
