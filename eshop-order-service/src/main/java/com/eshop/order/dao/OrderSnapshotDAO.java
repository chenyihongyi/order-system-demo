package com.eshop.order.dao;

import com.eshop.common.dao.BaseDAO;
import com.eshop.order.domain.entity.OrderSnapshotDO;
import com.eshop.order.mapper.OrderSnapshotMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单快照表 DAO
 * @date 2022-04-05
 */
@Repository
public class OrderSnapshotDAO extends BaseDAO<OrderSnapshotMapper, OrderSnapshotDO> {
}
