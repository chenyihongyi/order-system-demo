package com.eshop.order.dao;

import com.eshop.common.dao.BaseDAO;
import com.eshop.order.domain.entity.OrderAmountDetailDO;
import com.eshop.order.mapper.OrderAmountDetailMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单价格明细表 DAO
 * @date 2022-04-05
 */
@Repository
public class OrderAmountDetailDAO extends BaseDAO<OrderAmountDetailMapper, OrderAmountDetailDO> {
}
