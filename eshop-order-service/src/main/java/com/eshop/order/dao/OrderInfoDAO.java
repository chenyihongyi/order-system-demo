package com.eshop.order.dao;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.common.enums.DeleteStatusEnum;
import com.eshop.common.enums.OrderStatusEnum;
import com.eshop.order.domain.dto.OrderExtJsonDTO;
import com.eshop.order.domain.entity.OrderInfoDO;
import com.eshop.order.mapper.OrderInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单DAO
 * @date 2022-04-05
 */
@Repository
@Slf4j
public class OrderInfoDAO extends BaseDAO<OrderInfoMapper, OrderInfoDO> {

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    /**
     * 根据订单号查询订单
     *
     * @param orderId
     * @return
     */
    public OrderInfoDO getByOrderId(String orderId) {
        LambdaQueryWrapper<OrderInfoDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfoDO::getOrderId, orderId);
        return getOne(queryWrapper);
    }

    /**
     * 根据父订单号查询子订单号
     *
     * @param orderId
     * @return
     */
    public List<OrderInfoDO> listByParentOrderId(String orderId) {
        LambdaQueryWrapper<OrderInfoDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderInfoDO::getParentOrderId, orderId);
        return list(queryWrapper);
    }

    /**
     * 根据订单号查询订单号
     *
     * @param orderIds
     * @return
     */
    public List<OrderInfoDO> listByOrderIds(List<String> orderIds) {
        LambdaQueryWrapper<OrderInfoDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(OrderInfoDO::getOrderId, orderIds);
        return list(queryWrapper);
    }

    /**
     * 软删除订单
     *
     * @param ids 订单主键id
     */
    public void softRemoveOrders(List<Long> ids) {
        LambdaUpdateWrapper<OrderInfoDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(OrderInfoDO::getDeleteStatus, DeleteStatusEnum.YES.getCode())
                .in(OrderInfoDO::getId, ids);
        this.update(updateWrapper);
    }

    /**
     * 更新订单扩展信息
     *
     * @param orderId
     * @return
     */
    public boolean updateOrderExtJson(String orderId, OrderExtJsonDTO extJson) {
        String extJsonStr = JSONObject.toJSONString(extJson);
        LambdaUpdateWrapper<OrderInfoDO> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.set(OrderInfoDO::getExtJson, extJsonStr)
                .eq(OrderInfoDO::getOrderId, orderId);
        return this.update(updateWrapper);
    }

    public boolean updateOrderInfo(OrderInfoDO orderInfoDO) {
        UpdateWrapper<OrderInfoDO> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("order_id", orderInfoDO.getOrderId());
        return update(orderInfoDO, updateWrapper);
    }

    public boolean updateOrderStatus(String orderId, Integer fromStatus, Integer toStatus) {
        LambdaUpdateWrapper<OrderInfoDO> updateWrapper = new LambdaUpdateWrapper<>();

        updateWrapper.set(OrderInfoDO::getOrderStatus, toStatus)
                .eq(OrderInfoDO::getOrderId, orderId)
                .eq(OrderInfoDO::getOrderStatus, fromStatus);

        return update(updateWrapper);
    }

    /**
     * 扫描所有未支付订单
     *
     * @return
     */
    public List<OrderInfoDO> listAllUnPaid() {
        LambdaQueryWrapper<OrderInfoDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(OrderInfoDO::getOrderStatus, OrderStatusEnum.unPaidStatus());
        return list(queryWrapper);
    }
}
