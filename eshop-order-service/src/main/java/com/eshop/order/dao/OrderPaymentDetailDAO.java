package com.eshop.order.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.order.domain.entity.OrderPaymentDetailDO;
import com.eshop.order.mapper.OrderPaymentDetailMapper;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单支付明细表 DAO
 * @date 2022-04-05
 */
@Repository
public class OrderPaymentDetailDAO extends BaseDAO<OrderPaymentDetailMapper, OrderPaymentDetailDO> {

    /**
     * 查询订单支付明细
     */
    public OrderPaymentDetailDO getPaymentDetailByOrderId(String orderId) {
        QueryWrapper<OrderPaymentDetailDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId);
        return getOne(queryWrapper);
    }
}
