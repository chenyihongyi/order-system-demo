package com.eshop.order.service;

import com.eshop.order.domain.dto.WmsShipDTO;
import com.eshop.order.exception.OrderBizException;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单履约相关service
 * @date 2022-04-06
 */
public interface OrderFulFillService {

    /**
     * 触发订单进行履约流程
     * @param orderId
     * @return
     */
    void triggerOrderFulFill(String orderId) throws OrderBizException;

    /**
     * 通知订单物流配送结果接口
     * @return
     */
    void informOrderWmsShipResult(WmsShipDTO wmsShipDTO) throws OrderBizException;
}
