package com.eshop.order.service.impl;

import com.eshop.order.dao.AfterSaleLogDAO;
import com.eshop.order.domain.entity.AfterSaleInfoDO;
import com.eshop.order.domain.entity.AfterSaleLogDO;
import com.eshop.order.enums.AfterSaleStatusChangeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author Elvis
 * @version 1.0
 * @description:售后单操作日志工厂
 * @date 2022-04-06
 */
@Component
public class AfterSaleOperateLogFactory {

    @Autowired
    private AfterSaleLogDAO afterSaleLogDAO;

    /**
     * 获取售后操作日志
     */
    public AfterSaleLogDO get(AfterSaleInfoDO afterSaleInfo, AfterSaleStatusChangeEnum statusChange){
        String operateRemark = statusChange.getOperateRemark();
        Integer preStatus = statusChange.getPreStatus().getCode();
        Integer currentStatus = statusChange.getCurrentStatus().getCode();
        return create(afterSaleInfo, preStatus,currentStatus,operateRemark);
    }

    /**
     * 创建售后单操作日志
     * @param afterSaleInfo
     * @param preStatus
     * @param currentStatus
     * @param operateRemark
     * @return
     * @throws Exception
     */
    private AfterSaleLogDO create(AfterSaleInfoDO afterSaleInfo ,int preStatus,int currentStatus, String operateRemark) {
        AfterSaleLogDO log = new AfterSaleLogDO();

        log.setAfterSaleId(String.valueOf(afterSaleInfo.getAfterSaleId()));
        log.setPreStatus(preStatus);
        log.setCurrentStatus(currentStatus);
        log.setRemark(operateRemark);

        return log;
    }


}
