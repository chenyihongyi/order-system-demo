package com.eshop.order.service;

import com.eshop.order.domain.dto.CheckLackDTO;
import com.eshop.order.domain.dto.LackDTO;
import com.eshop.order.domain.entity.OrderInfoDO;
import com.eshop.order.domain.request.LackRequest;
import com.eshop.order.exception.OrderBizException;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单缺品相关service
 * @date 2022-04-08
 */
public interface OrderLackService {

    /**
     * 校验入参
     * @param request
     */
    CheckLackDTO checkRequest(LackRequest request) throws OrderBizException;

    /**
     * 订单是否已经发起过缺品
     * @param order
     * @return
     */
    boolean isOrderLacked(OrderInfoDO order);

    /**
     * 具体的缺品处理
     * @param request
     * @param checkLackDTO
     * @return
     */
    LackDTO executeLackRequest(LackRequest request, CheckLackDTO checkLackDTO);
}
