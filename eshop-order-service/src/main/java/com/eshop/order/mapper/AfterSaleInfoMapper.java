package com.eshop.order.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.order.dao.AfterSaleListQueryDTO;
import com.eshop.order.domain.dto.AfterSaleOrderListDTO;
import com.eshop.order.domain.entity.AfterSaleInfoDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-06
 */
@Mapper
public interface AfterSaleInfoMapper extends BaseMapper<AfterSaleInfoDO> {

    /**
     * 售后单分页查询
     * @param query
     * @return
     */
    Page<AfterSaleOrderListDTO> listByPage(Page<AfterSaleOrderListDTO> page, @Param("query") AfterSaleListQueryDTO query);

}
