package com.eshop.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.order.domain.entity.OrderItemDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 订单条目表 Mapper 接口
 * @date 2022-04-05
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItemDO> {

}
