package com.eshop.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.order.domain.entity.AfterSaleItemDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 订单售后条目表 Mapper 接口
 * </p>
 *
 * @author zhonghuashishan
 */
@Mapper
public interface AfterSaleItemMapper extends BaseMapper<AfterSaleItemDO> {

}
