package com.eshop.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.order.domain.entity.AfterSaleRefundDO;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author Elvis
 * @version 1.0
 * @description:售后支付表 Mapper 接口
 * @date 2022-04-06
 */
@Mapper
public interface AfterSaleRefundMapper extends BaseMapper<AfterSaleRefundDO> {

}
