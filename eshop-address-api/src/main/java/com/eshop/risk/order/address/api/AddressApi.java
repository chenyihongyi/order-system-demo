package com.eshop.risk.order.address.api;

import com.eshop.risk.order.address.domain.dto.AddressDTO;
import com.eshop.risk.order.address.domain.query.AddressQuery;
import com.eshop.common.core.JsonResult;

/**
 * @author Elvis
 * @version 1.0
 * @description: 地址服务业务接口
 * @date 2022-04-05
 */
public interface AddressApi {

    /**
     * 查询地址
     * @return
     */
    JsonResult<AddressDTO> queryAddress(AddressQuery query);
}
