package com.eshop.tms.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.tms.domain.entity.LogisticOrderDO;
import com.eshop.tms.mapper.LogisticOrderMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流单 DAO
 * @date 2022-04-08
 */
@Repository
public class LogisticOrderDAO extends BaseDAO<LogisticOrderMapper, LogisticOrderDO> {

    /**
     * 查询物流单
     * @author zhonghuashishan
     * @version 1.0
     */
    public List<LogisticOrderDO> listByOrderId(String orderId) {
        LambdaQueryWrapper<LogisticOrderDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(LogisticOrderDO::getOrderId,orderId);
        return list(queryWrapper);
    }

}
