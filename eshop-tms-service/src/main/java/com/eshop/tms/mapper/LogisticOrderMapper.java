package com.eshop.tms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.tms.domain.entity.LogisticOrderDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description: 物流单 Mapper 接口
 * @date 2022-04-08
 */
@Mapper
public interface LogisticOrderMapper extends BaseMapper<LogisticOrderDO> {

}
