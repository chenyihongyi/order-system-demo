package com.eshop.tms.exception;

import com.eshop.common.exception.BaseBizException;
import com.eshop.common.exception.BaseErrorCodeEnum;

/**
 * @author Elvis
 * @version 1.0
 * @description: tms中心自定义业务异常类
 * @date 2022-04-08
 */
public class TmsBizException extends BaseBizException {

    public TmsBizException(String errorMsg) {
        super(errorMsg);
    }

    public TmsBizException(String errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public TmsBizException(BaseErrorCodeEnum baseErrorCodeEnum) {
        super(baseErrorCodeEnum);
    }

    public TmsBizException(String errorCode, String errorMsg, Object... arguments) {
        super(errorCode, errorMsg, arguments);
    }

    public TmsBizException(BaseErrorCodeEnum baseErrorCodeEnum, Object... arguments) {
        super(baseErrorCodeEnum, arguments);
    }
}
