package com.eshop.risk.api.impl;

import com.eshop.common.core.JsonResult;
import com.eshop.risk.api.RiskApi;
import com.eshop.risk.domain.dto.CheckOrderRiskDTO;
import com.eshop.risk.domain.CheckOrderRiskRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = RiskApi.class)
public class RiskApiImpl implements RiskApi {

    @Override
    public JsonResult<CheckOrderRiskDTO> checkOrderRisk(CheckOrderRiskRequest checkOrderRiskRequest) {
        // 执行风控检查 TODO
        CheckOrderRiskDTO checkOrderRiskDTO = new CheckOrderRiskDTO();
        checkOrderRiskDTO.setResult(true);
        // 默认风控检查通过
        return JsonResult.buildSuccess(checkOrderRiskDTO);
    }
}
