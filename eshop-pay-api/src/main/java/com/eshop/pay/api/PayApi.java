package com.eshop.pay.api;

import com.eshop.common.core.JsonResult;
import com.eshop.pay.domain.dto.PayOrderDTO;
import com.eshop.pay.domain.request.PayOrderRequest;
import com.eshop.pay.domain.request.PayRefundRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface PayApi {

    /**
     * 支付订单
     *
     * @param payOrderRequest
     * @return
     */
    JsonResult<PayOrderDTO> payOrder(PayOrderRequest payOrderRequest);

    /**
     * 查询支付交易流水号
     */
    Boolean getTradeNoByRealTime(String orderId, Integer businessIdentifier);

    /**
     * 调用支付接口执行退款
     */
    Boolean executeRefund(PayRefundRequest payRefundRequest);
}