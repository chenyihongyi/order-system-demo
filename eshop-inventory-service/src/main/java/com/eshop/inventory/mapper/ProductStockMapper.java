package com.eshop.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.inventory.domain.entity.ProductStockDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Elvis
 * @version 1.0
 * @description: 库存中心的商品库存表 Mapper 接口
 * @date 2022-04-05
 */
@Mapper
public interface ProductStockMapper extends BaseMapper<ProductStockDO> {

    /**
     * 锁定商品库存
     * @param skuCode
     * @param saleQuantity
     * @return
     */
    int lockProductStock(@Param("skuCode") String skuCode, @Param("saleQuantity") Integer saleQuantity);

    /**
     * 释放商品库存
     * @param skuCode
     * @param saleQuantity
     * @return
     */
    int releaseProductStock(@Param("skuCode") String skuCode, @Param("saleQuantity") Integer saleQuantity);
}
