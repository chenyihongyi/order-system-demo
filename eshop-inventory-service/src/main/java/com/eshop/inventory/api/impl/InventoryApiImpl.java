package com.eshop.inventory.api.impl;

import com.eshop.common.core.JsonResult;
import com.eshop.inventory.api.InventoryApi;
import com.eshop.inventory.domain.request.LockProductStockRequest;
import com.eshop.inventory.exception.InventoryBizException;
import com.eshop.inventory.service.InventoryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@DubboService(version = "1.0.0", interfaceClass = InventoryApi.class, retries = 0)
@Slf4j
public class InventoryApiImpl implements InventoryApi {

    @Autowired
    private InventoryService inventoryService;

    /**
     * 锁定商品库存
     *
     * @param lockProductStockRequest
     * @return
     */
    @Override
    public JsonResult<Boolean> lockProductStock(LockProductStockRequest lockProductStockRequest) {
        try {
            Boolean result = inventoryService.lockProductStock(lockProductStockRequest);
            return JsonResult.buildSuccess(result);
        } catch (InventoryBizException e) {
            log.error("biz error", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
