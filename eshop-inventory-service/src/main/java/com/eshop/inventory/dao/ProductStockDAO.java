package com.eshop.inventory.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.eshop.common.dao.BaseDAO;
import com.eshop.inventory.domain.entity.ProductStockDO;
import com.eshop.inventory.mapper.ProductStockMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Elvis
 * @version 1.0
 * @description: 库存中心的商品库存表 Mapper 接口
 * @date 2022-04-05
 */
@Repository
public class ProductStockDAO extends BaseDAO<ProductStockMapper, ProductStockDO> {

    @Autowired
    private ProductStockMapper productStockMapper;

    /**
     * 根据skuCode查询商品库存记录
     * @param skuCode
     * @return
     */
    public ProductStockDO getBySkuCode(String skuCode) {
        QueryWrapper<ProductStockDO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sku_code", skuCode);
        return getOne(queryWrapper);
    }

    /**
     * 锁定商品库存
     * @param skuCode
     * @param saleQuantity
     * @return
     */
    public int lockProductStock(String skuCode, Integer saleQuantity) {
        return productStockMapper.lockProductStock(skuCode, saleQuantity);
    }
}
