package com.eshop.inventory.service.impl;

import com.eshop.common.utils.ParamCheckUtil;
import com.eshop.inventory.dao.ProductStockDAO;
import com.eshop.inventory.domain.entity.ProductStockDO;
import com.eshop.inventory.domain.request.LockProductStockRequest;
import com.eshop.inventory.exception.InventoryBizException;
import com.eshop.inventory.exception.InventoryErrorCodeEnum;
import com.eshop.inventory.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private ProductStockDAO productStockDAO;

    /**
     * 锁定商品库存
     * @param lockProductStockRequest
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean lockProductStock(LockProductStockRequest lockProductStockRequest) {
        // 检查入参
        checkLockProductStockRequest(lockProductStockRequest);

        List<LockProductStockRequest.OrderItemRequest> orderItemRequestList =
                lockProductStockRequest.getOrderItemRequestList();
        for(LockProductStockRequest.OrderItemRequest orderItemRequest : orderItemRequestList) {
            String skuCode = orderItemRequest.getSkuCode();
            ProductStockDO productStockDO = productStockDAO.getBySkuCode(skuCode);
            if(productStockDO == null) {
                throw new InventoryBizException(InventoryErrorCodeEnum.PRODUCT_SKU_STOCK_ERROR);
            }
            Integer saleQuantity = orderItemRequest.getSaleQuantity();
            // 执行库存扣减，并需要解决防止超卖的问题
            int nums = productStockDAO.lockProductStock(skuCode, saleQuantity);
            if(nums <= 0) {
                throw new InventoryBizException(InventoryErrorCodeEnum.LOCK_PRODUCT_SKU_STOCK_ERROR);
            }
        }
        return true;
    }

    /**
     * 检查锁定商品库存入参
     * @param lockProductStockRequest
     */
    private void checkLockProductStockRequest(LockProductStockRequest lockProductStockRequest) {
        String orderId = lockProductStockRequest.getOrderId();
        ParamCheckUtil.checkStringNonEmpty(orderId);
        List<LockProductStockRequest.OrderItemRequest> orderItemRequestList =
                lockProductStockRequest.getOrderItemRequestList();
        ParamCheckUtil.checkCollectionNonEmpty(orderItemRequestList);
    }
}
