package com.eshop.inventory.service;

import com.eshop.inventory.domain.request.LockProductStockRequest;

/**
 * @author Elvis
 * @version 1.0
 * @description:
 * @date 2022-04-05
 */
public interface InventoryService {

    /**
     * 锁定商品库存
     * @param lockProductStockRequest
     * @return
     */
    Boolean lockProductStock(LockProductStockRequest lockProductStockRequest);
}
