package com.eshop.inventory.exception;

import com.eshop.common.exception.BaseBizException;
import com.eshop.common.exception.BaseErrorCodeEnum;

/**
 * @author Elvis
 * @version 1.0
 * @description:库存服务自定义业务异常
 * @date 2022-04-05
 */
public class InventoryBizException extends BaseBizException {

    public InventoryBizException(String errorMsg) {
        super(errorMsg);
    }

    public InventoryBizException(String errorCode, String errorMsg) {
        super(errorCode, errorMsg);
    }

    public InventoryBizException(BaseErrorCodeEnum baseErrorCodeEnum) {
        super(baseErrorCodeEnum);
    }

    public InventoryBizException(String errorCode, String errorMsg, Object... arguments) {
        super(errorCode, errorMsg, arguments);
    }

    public InventoryBizException(BaseErrorCodeEnum baseErrorCodeEnum, Object... arguments) {
        super(baseErrorCodeEnum, arguments);
    }
}
