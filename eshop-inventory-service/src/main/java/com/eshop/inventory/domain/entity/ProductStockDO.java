package com.eshop.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.eshop.common.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Elvis
 * @version 1.0
 * @description: 库存中心的商品库存表
 * @date 2022-04-05
 */
@Data
@TableName("inventory_product_stock")
public class ProductStockDO extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品sku编号
     */
    private Long skuCode;

    /**
     * 销售库存
     */
    private Long saleStockQuantity;

    /**
     * 锁定库存
     */
    private Long lockedStockQuantity;
}
