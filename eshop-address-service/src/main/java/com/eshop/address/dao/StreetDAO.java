package com.eshop.address.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.eshop.address.domain.entity.StreetDO;
import com.eshop.address.mapper.StreetMapper;
import com.eshop.common.dao.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author Elvis
 * @version 1.0
 * @description:街道 DAO
 * @date 2022-04-06
 */
@Repository
public class StreetDAO extends BaseDAO<StreetMapper, StreetDO> {

    /**
     * 查询街道信息
     * @param streetCode
     * @param street
     * @return
     */
    public List<StreetDO> listStreets(String streetCode, String street) {
        LambdaQueryWrapper<StreetDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(streetCode),StreetDO::getCode,streetCode)
                .eq(StringUtils.isNotBlank(street),StreetDO::getName,street);
        return list(queryWrapper);
    }
}
