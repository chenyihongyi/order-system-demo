package com.eshop.address.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.eshop.address.domain.entity.AreaDO;
import com.eshop.address.mapper.AreaMapper;
import com.eshop.common.dao.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author Elvis
 * @version 1.0
 * @description:区域 DAO
 * @date 2022-04-06
 */
@Repository
public class AreaDAO extends BaseDAO<AreaMapper, AreaDO> {


    /**
     * 查询地区
     * @param areaCodes
     * @param area
     * @return
     */
    public List<AreaDO> listAreas(Set<String> areaCodes, String area) {
        LambdaQueryWrapper<AreaDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(area),AreaDO::getName,area)
                .in(CollectionUtils.isNotEmpty(areaCodes),AreaDO::getCode,areaCodes);
        return list(queryWrapper);
    }
}
