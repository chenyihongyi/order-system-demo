package com.eshop.address.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.eshop.address.domain.entity.ProvinceDO;
import com.eshop.address.mapper.ProvinceMapper;
import com.eshop.common.dao.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author Elvis
 * @version 1.0
 * @description:省 DAO
 * @date 2022-04-06
 */
@Repository
public class ProvinceDAO extends BaseDAO<ProvinceMapper, ProvinceDO> {

    public List<ProvinceDO> listProvinces(Set<String> provinceCodes, String province) {
        LambdaQueryWrapper<ProvinceDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(province),ProvinceDO::getName,province)
                .in(CollectionUtils.isNotEmpty(provinceCodes),ProvinceDO::getCode,provinceCodes);
        return list(queryWrapper);
    }
}
