package com.eshop.address.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.eshop.address.domain.entity.CityDO;
import com.eshop.address.mapper.CityMapper;
import com.eshop.common.dao.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author Elvis
 * @version 1.0
 * @description:市 DAO
 * @date 2022-04-06
 */
@Repository
public class CityDAO extends BaseDAO<CityMapper, CityDO> {

    /**
     * 查询市
     * @param cityCodes
     * @param city
     * @return
     */
    public List<CityDO> listCities(Set<String> cityCodes, String city) {
        LambdaQueryWrapper<CityDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(city),CityDO::getName,city)
                .in(CollectionUtils.isNotEmpty(cityCodes),CityDO::getCode,cityCodes);
        return list(queryWrapper);
    }
}
