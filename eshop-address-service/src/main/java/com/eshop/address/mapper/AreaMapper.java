package com.eshop.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.address.domain.entity.AreaDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Elvis
 * @version 1.0
 * @description:地区设置 Mapper 接口
 * @date 2022-04-06
 */
@Mapper
public interface AreaMapper extends BaseMapper<AreaDO> {

}
