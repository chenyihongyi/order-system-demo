package com.eshop.address.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eshop.address.domain.entity.CityDO;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author Elvis
 * @version 1.0
 * @description:城市设置 Mapper 接口
 * @date 2022-04-06
 */
@Mapper
public interface CityMapper extends BaseMapper<CityDO> {

}
